// Bài 1: Tìm số nguyên dương nhỏ nhất
var tongSoNguyenDuong = 0;
var soNguyenDuongNhoNhat = 0;
for (var i = 1; i < 10000; i++) {
    tongSoNguyenDuong += i;
    if (tongSoNguyenDuong > 10000) {
        soNguyenDuongNhoNhat = i;
        break;
    }
}
document.querySelector("#soNguyenDuongNhoNhat").innerHTML = `Số nguyên dương nhỏ nhất: ${soNguyenDuongNhoNhat}`

// Bài 2: Tính tổng 

document.querySelector('#tinhTong').onclick = function () {
    var tong = 0;
    const soX = document.getElementById('soX').value * 1;
    const soN = document.getElementById('soN').value * 1;
    const ketqua = document.getElementById('tong');
    for (var j = 1; j <= soN; j++) {
        tong += Math.pow(soX, j);
    }
    ketqua.innerHTML = `Kết quả: S(n) = ${tong}`
}

// Bài 3: Tính giai thừa 
document.querySelector('#tinhGiaiThua').onclick = function () {
    var tinhGiaiThua = 1;
    const soN1 = document.getElementById('soN1').value * 1;
    for (var k = 1; k <= soN1; k++) {
        tinhGiaiThua *= k;
    }
    document.querySelector('#giaiThua').innerHTML = `Giai thừa: ${tinhGiaiThua}`
};

// Bài 4: Tạo div 
document.querySelector('#taoDiv').onclick = function () {
    var tongDiv = ""
    for (var l = 1; l <= 10; l++) {
        if (l % 2 == 0) {
            tongDiv += `<div class="bg-danger text-light p-2">Div chẵn ${l}</div>`
        } else {
            tongDiv += `<div class="bg-primary text-light p-2">Div lẻ ${l}</div>`
        }
    }
    document.querySelector('#div').innerHTML = tongDiv
}

// Bài 5: In số nguyên tố 
document.querySelector('#inSo').onclick = function () {
    var soNguyenTo = ''
    const soN2 = document.getElementById('soN2').value * 1;
    for (var j = 2; j <= soN2; j++) {
        var isNguyenTo = true
        for (var i = 2; i <= Math.sqrt(j); i++) {
            if (j % i === 0) {
                isNguyenTo = false;
                break;
            }   
        }
        if (isNguyenTo) {
            soNguyenTo += j + " "
        }
    }
    document.getElementById('inSoNguyenTo').innerHTML = `Số nguyên tố: ${soNguyenTo}`
}